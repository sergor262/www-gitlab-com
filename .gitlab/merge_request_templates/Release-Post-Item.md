<!-- SET THE RIGHT LABELS AND MILESTONE (let the autocomplete guide you) -->

/label ~"release post" ~"release post item" ~"technical writing" ~"devops::" ~"group::"
/milestone %
/assign `@PM`

Engineer(s): `@engineers` | Product Marketing: `@PMM` | Tech Writer: `@techwriter` | Engineering Manager: `@EM`

Please review the guidelines for feature block creation at https://about.gitlab.com/handbook/marketing/blog/release-posts/#feature-blocks.
They are frequently updated, and everyone should make sure they are aware of the current standards (PM, PMM, EM, and TW).

## Links

- Feature Issue:
- Feature MR (optional):
- Release post (optional):

## When to merge

**Reminder: Make sure any feature flags have been enabled or removed!**

Once all content is reviewed and complete, add the ~"Ready" label and assign this issue to the Engineering Manager (EM). The EM is responsible for merging as soon as the implementing issue is deployed to GitLab.com, after which this content will appear on the [GitLab.com Release page](https://about.gitlab.com/releases/gitlab-com/) and can be included in the next release post. All release post items must be merged on or before the 17th of the month. If a feature is not ready by the 17th deadline, the EM should push the release post item to the next milestone.

## PM release post item checklist

- [ ] Structure
  - Screenshot/video is included (optional for secondary items).
  - Check that [image size < 150KB](https://about.gitlab.com/handbook/marketing/blog/release-posts/#images).
  - Check if the image shadow is applied correctly. Add `image_noshadow: true` when an image already has a shadow.
  - Remove any remaining instructions (comments).
- [ ] Frontmatter
  - Check feature availability frontmatter (`available_in:`) is correct: (Core, Starter, Premium, Ultimate).
  - Ensure videos and iframes added within the feature description are wrapped in `<figure class="video_container">` tags (for responsiveness).
  - Check documentation link points to the latest docs (`documentation_link:`), and includes the anchor to the relevant section on the page if possible. If documentation is not yet available/merged for the feature in question, you may use a placeholder or use the link where the documentation will be added (often the engineer and tech writer know this ahead of time). Be sure to update this placeholder prior to publication if you do not use the final link.
  - Check that all links to `about.gitlab.com` content are relative URLs.
- [ ] Content
  - Ensure the **why** is clearly explained: what is the problem we are solving for the user, and what value are we delivering?
  - Make it clear if it is a new feature, or an improvement to an existing feature.
  - Check title is in sentence case, and feature and product names are in capital case.
  - Check that documentation is updated, very clearly talks about the feature (mentions it by name).
  - Run the content through an automated spelling and grammar check.
  - Validate all links are functional and have [meaningful text](https://about.gitlab.com/handbook/communication/#writing-style-guidelines) for SEO (e.g., "click here" is bad link text).
- [ ] Data
  - Feature is added to [`data/features.yml`](https://about.gitlab.com/handbook/marketing/website/#adding-features-to-webpages) (with accompanying screenshots).

## Review

When the above is complete and the content is ready for review, it should be reviewed by Product Marketing, Tech Writing,
and the product director for this area. Please assign them when it is ready to be reviewed!

- [ ] Tech writer, PMM, and relevant product director assigned for review
  - [ ] Tech writer reviewed and approved
  - [ ] PMM reviewed and approved
  - [ ] Group Manager or Director reviewed and approved

### Tech writing review
*(required)*

- [ ] Verify Frontmatter:
  - Check that all links and URLs are wrapped in single quotes `'` (`documentation_link: 'https://docs.gitlab.com/ee/#amazing'`).
  - Check that name fields are wrapped in double quotes `"` (`name: "Lorem ipsum"`).
  - No useless whitespace (end of line spaces, double spaces, extra blank lines, and lines with only spaces).
- [ ] Links
  - Verify all links (including in feature description) work and that anchors are valid (note: the H1 (top) anchor on a docs page is not valid). Links should not redirect.
  - Verify that the feature is clearly mentioned in the linked documentation. If not, suggest a better doc link. If none exists, note this and raise an issue for creation of documentation if needed.
- [ ] Content
  - Check that there are no typos or grammar mistakes, but style is up to the PM and PMM.
  - Ignoring style, verify that content accurately describes the feature based on your understanding of it.

### Director review
*(recommended)*

- [ ] Content
  - Ensure the **why** is clearly explained: what is the problem we are solving for the user, and what value are we delivering?

### PMM review 
*(recommended)*

- [ ] PMM review
  - [ ] **problem/solution**: Does this describe the user pain points (problem) as well as how the new feature removes the paint points (solves the problem)? 
  - [ ] **short/pithy:** Is this communicated clearly with the fewest words possible?
  - [ ] **tone clarify:** Is the language and sentence structure clear and grammatically correct?
  - [ ] **technical clarity**: Does the descripton of the feature make sense for various audiences, including folks who are not deeply familiar with GitLab? 
