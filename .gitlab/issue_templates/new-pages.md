
## This template is for requesting new pages.

This repository primarily relates to the logged out [marketing website](https://about.gitlab.com/) and [company handbook](https://about.gitlab.com/handbook/). This is not the right repository for requests related to docs.gitlab.com, product, or other parts of the site.

# Issues should identify [The Five W's](https://en.wikipedia.org/wiki/Five_Ws) : who, what, when, where, and why.

It is also recommended to apply [MoSCoW](https://en.wikipedia.org/wiki/MoSCoW_method) sorting to your requests (must, should, could, won't).

#### Please apply appropriate labels or your issue may not be sorted on to appropriate boards.

Please refer to the [website label documentation](/handbook/marketing/brand-and-digital-design/#issue-labels)

## Please delete the lines above and any irrelevant sections below.

Below are suggestions. Any extra information you provide is beneficial.

#### What is/are the expected URL(s)

Does this match existing strategies? Has keyword research been done?

#### Briefly describe the page/flow

(Example: Add a new page comparing us to a specific competitor)

#### If applicable, do you have any user journeys, wireframes, prototypes, content/data?

(Example: Google doc/sheet, balsamiq, etc)

#### What is the primary purpose of this page/flow?

(Example: Increase EE trials)

#### What are the KPI (Key Performance Indicators)

(Example: Premium signups per month as identified by Periscope dashboard URL)

#### What other purposes does this page/flow serve, if any?

(Example: Capture emails for campaign funnels)

#### What information is this page/flow trying to convey?

(Example: Forrester says GitLab is great)

### Who is the primary audience? Be specific.

(Example: Potential customers vs developers vs CTO vs Jenkins users)

#### Any other audiences?

(Example: Media outlets, investors, etc)

#### Where do you expect traffic to come from?

(Example: Homepage direct link, on-site blog post, social media campaign, targeted ads)

#### If applicable, are there any items which can be hidden at first glance but still need to be present?

(Example: Tooltips, tabbed sections, collapsible sections, filterable items, different view-type toggles, view-more buttons, extra data)

#### Please provide some example websites for design guidance.

(Example: "Pinterest implements recommendations well", "Stripe has good payment UX", "I like the aesthetics of nike.com", animation examples, microinteractions, etc)

#### Does this page/flow require any events, parameters, or tracking?

(Example: Clicking button X should record a purchase into analytics)

#### Does this page/flow impact or need any reports?

(Example: This impacts the Retention dashboard in Periscope)

#### Does this page/flow require any automation?

(Example: Create a test ensuring a job listing is never empty)

<!-- These labels will be automatically applied unless you edit or delete the following section -->
/label ~"mktg-website" ~"template::web-new" ~"mktg-status::triage"

<!-- If you do not want to ping the website team, please remove the following section -->
/cc @gl-website
