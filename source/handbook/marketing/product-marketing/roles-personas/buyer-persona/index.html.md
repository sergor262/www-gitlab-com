---
layout: handbook-page-toc
title: "Buyer Personas"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}


![IT Orgchart](https://about.gitlab.com/handbook/marketing/product-marketing/images/IT-Org.png)


### Alex - the Application Development Manager

#### Who - Demographics & Environment

* **Background:**  
   *  Front line leader who in first 'management' role.  
   *  Evolved from individual contributor to a team lead on projects, to full time 'management' role.  

* **Demographics:**  (need more data) 
   *  Typically male, with ~6-10 years of experience. 
   *  Income range depends on industry and location. 
   *  Mostly major metropolitan areas world-wide with development teams.

* **Identifiers** - Demeanor? Communication preferences?:  
   * Highly technical, and only one step removed from an individual contributor.  
   * Get into the details more than his leaders (directors and VPs).  
   * When evaluating solutions, likely to explore product demos and dig into the technical details.

* **Company** - Market segment/industry? Size? On-site, distributed? Full Remote?  
    * Works in major metropolitan areas around the world. 
    * Often managing a development teams comprised of employees and external firms (on-site, distributed or remote).

* **Team Details:** Size? Development Methodology? DevOps adoption path?  
    * Manages team of 8 to 12 developers using Agile to plan and manage their work.  
    * In the process of automating development processes for building and testing their application(DevOps).

* **Influencers- where do they learn:** 
    * Reads SD Times, DevOps.com, Hackernoon, StackOverflow, and others.  
    * Frequently checks out CIO Magazine for leadership and bigger picture challenges. 
    * Because his organization is focused on cloud transformation, he attends AWS conferences and is actively following key webinars that help him learn about how he can better apply technology to meet his business's demands.

#### What - Motivations

* **Goals/motivations:**  - His goals are tied to several dimensions.  
    * *Business Satisfaction* is a key goal, as he manages the IT-Business relationship for the team. 
    * Working with the business to prioritize the backlog and determine what user stories will be in the upcoming sprint.  
    * Managing his budget and ensuring that his team has the right skills and overall team engagement.   
    * Reinforce the importance of key SDLC and process tasks ranging form quality, documenting code reviews, security, and accurate time tracking.

* **Challenges:** - Alex is looking for help solving his challenges:
    * He never has enough resources (time, people, or infrastructure) to deliver on all the competing requests. 
    * Balancing new features against technical debt acrued during prior development. 
    * Balancing scope, budget, resources, and expectations on a daily basis. 
    * Lack of accurate and real time status of individual work, leading to him having to disrupt team members with questions.
    * Persuading other stakeholders to consider and purchase new tools. 
    * Understanding potential negatives of adding new tools to existing toolsets.

* **What can we do to help them achieve goals and overcome their challenges?**
1. Streamline team collaboration and increase team productivity to meet business needs and expectations.
1. Simplify tracking and monitoring of their teams work. With GitLab, they are able to see the actual status of issues and merge requests.
1. Productivity and performance metrics help them improve their teams performance.
1. Compliance and reporting.
2. Make it easy for them understand how GitLab solves their challenges, and how to adopt with minimal risk/disruption.
3. Give them compelling content to pursuade others in their organization to choose GitLab.

* **Why do we want to connect** Depending on the size of the company where he works, his role in the tool selection process will vary.  
- **SMB**: Likely responsible for the tool selection decision and has budget authority for his team's tools.  
- **MidSize**: Significant influence over the overall tool selection process, however may not be the final purchasing authority.
- **Large** (enterprise): In a large enterprise, there are multiple development teams and development managers.   In *some* situations, they may have autonomy to select tools for their team, however it is more common for teams to have tool purchases centrally managed, and the **Dev Manager** will play a key role in *recommending* a solution, but will not be the final buyer.


* Real Quotes - Examples of things they commonly say about their goals and challenges.
(Need to gather data)

* **Common Objections** - Why wouldn't they buy us?
1. It's not their 'corporate standard', they have other standard tools, and it's above their pay grade to change.
1. They need to justify the effort to convert from an existing legacy tool to GitLab.  The purchase is only one part of their business case, and they need to justify the labor as much as the OPEX they are spending.
1. They are not responsible for the full tool chain, in fact, they may not 'own' any of the end to end tools
1. They don't have a large tool budget, it's a shared function or managed higher up in the organization.

#### Why - Messaging

* **Discover Questions** - questions to ask
1. "Tell me about the business your team supports" - Learn about the underlying business system and technology they support.
1. "What challenges are you facing with keeping up with business requests?"  - Almost ALL development managers don't have enough resources to keep up and are always trying to prioritize what next.
1. "How are you measuring success, are there strategic goals to accelerate delivery or improve efficiency?" - What kind of challenges are they facing
Note: need to add a few more from Command of the Message

* **Messaging** - How describe our product and value drivers to this persona?
1. GitLab is a key to unlocking the true potential of your team to help you meet the needs of your business partners.  GitLab helps teams become more efficient and effective and stop wasting time waiting.  It is easy for developers to maintain context of work from requirements to code to testing and even deployment.  
1. Improve visibility and insight so you can know the true status of deliverables without disrupting your team.
1. Streamline developer on boarding and bring new team members up to speed faster and more efficiently.

* **Elevator pitch** - What's our 1-2 sentence pitch to this persona?
- Help your team become more effective and efficient. Enable them to collaborate and accelerate application delivery, eliminating wasted time switching context from one tool to another, **and** get data and insight into how the team is performing.


---

### Dakota - the Application Development Director
#### Who - Demographics & Environment

* **Background:**  Dakota is a key IT leader who manages and leads several teams of developers supporting a specific set of business applications.  She has both technical and business skills and as a manager she's focused on delivering business innovation.

* **Demographics:**  (need more data) Typically male, with ~9-15 years of experience. Income range depends on industry and location. They can be found in all most all major metropolitan areas where there are development teams.

* **Identifiers** - Demeanor? Communication preferences?:  Dakota is removed from day to day technical details and focuses her days on planning future work, team skill development, business relationships, and overall team performance. She is concerned about cost and feasibility, managing risk, and leading organizational change. When evaluating solutions, she is more likely to focus on value over specific features.  She will rely on her team to dig into the technical details and demos, etc.

* **Company** - Market segment/industry? Size? On-site, distributed? Full Remote?  Dakota can work just about anywhere, in most major metropolitan areas around the world.   She will typically be in Mid size to large organizations where they have a significant IT and Developer team. There are opportunities for remote work, and often he's managing a blended team of developers who are both his employees and developers from external firms.

* **Team Details:** Size? Development Methodology? DevOps adoption path?  She manages 3 to 5 managers, each who has from 8 to 12 team members. They are in the process of automating development processes for building and testing their application(DevOps).

* **Influencers- where do they learn:** Dakota reads CIO Magazine, Gartner, Forrester and other analyst reports. She attends both industry specific conferences and also strategic technology  conferences such as Forrester, Gartner, and others. She is actively looking for ideas and insight into how to improve the efficiency and effectiveness of her teams.

#### What - Motivations

* **Goals/motivations:**  Her top goal is *Business Satisfaction*, and regardless of the technology, she wants to deliver consistent and predictable business results. She has had a long relationship with her business unit and feels as if she is an extension their team. She is responsible for the IT strategy for her business unit and is looking at ways to improve the business value of her systems.  Through her managers, she is responsible for growing and improving her team and how they improve their delivery effectiveness. She manages her team's budget, ensuring that they have the tools, training, and resources to be successfully develop and deliver the business value.

* **Challenges:** She balances her time between strategic planning with her business partners, and also resolving organizational issues and roadblocks her teams are facing. She develops organizational strategies and plans to secure budget and resources for her team.     

* **What can we do to help them achieve goals and overcome their challenges?**
1. Streamline team collaboration and throughput, helping their team deliver more features the for the business
1. Simplify their job of tracking and monitoring progress of their team working on development.  With GitLab, they are able to see the actual status of issues and merge requests.
1. Productivity and performance metrics help them improve their teams performance
1. Compliance and reporting

* **Why do we want to connect** Depending on the size of the company where she works, her role in the tool selection process will vary.  
- **SMB**: NA
- **MidSize**: Significant influence over the overall tool selection process, and probably will have final purchasing authority.
- **Large** (enterprise): In a large enterprise, there are multiple development teams and development managers. In *some* situations, they may have autonomy to select tools for their teams, however it is more common for tools to be centrally managed, and the **Dev Director** will play a vital and central role in *endorsing and recommending* a solution, where they are the champion that drives the final selection and recommendation.  

* Real Quotes - Examples of things they commonly say about their goals and challenges.
(Need to gather data)

* **Common Objections** - Why wouldn't they buy us?
1. It's not their 'corporate standard', they have other standard tools, and they are not willing to take the risk to promote a change.
1. They need to justify the effort to convert from an existing legacy tool to GitLab. The purchase is only one part of their business case, and they need to justify the labor as much as the OPEX they are spending.
1. They are not responsible for the full tool chain, in fact, they may not 'own' any of the end to end tools.
1. They don't have a large tool budget, it's a shared function or managed higher up in the organization.

#### Why - Messaging

* **Discover Questions** - questions to ask
1. "Tell me about the business your teams support" - Learn about the underlying business system and technology they support.
1. "What challenges are you facing with keeping up with business requests?" - Almost ALL development managers don't have enough resources to keep up and are always trying to prioritize what next.
1. "How are you measuring success? Are your strategic goals to accelerate delivery or improve efficiency?" - What kind of challenges are they facing
Note: need to add a few more from Command of the Message

* **Messaging** - How describe our product and value drivers to this persona?
1. GitLab is a key to unlocking the true potential of your team to help you meet the needs of your business partners.  GitLab helps teams become more efficient and effective and stop wasting time waiting.  It is easy for developers to maintain context of work from requirements to code to testing and even deployment.  
1. Improve visibility and insight so you can know the true status of deliverables without disrupting your team.
1. Streamline developer on boarding and bring new team members up to speed faster and more efficiently.

* **Elevator pitch** - What's our 1-2 sentence pitch to this persona?
- Help your team become more effective and efficient. Enable them to collaborate and accelerate application delivery, eliminating wasted time switching context from one tool to another, **and** get data and insight into how the team is performing.

### Erin - the Application Development Executive (VP, etc.)

#### Who - Demographics & Environment

* **Background:**  Erin is senior IT and development executive, who leads a large team of developers who build and support a line of business. Her career has spanned a number of roles in different organizations, and is focused on strategy and ensuring the team has resources to succeed.

* **Demographics:**  (need more data) Typically male, with ~15-20 years of experience. Income range depends on industry and location. They can be found in all most all major metropolitan areas where there are development teams.

* **Identifiers** - Demeanor? Communication preferences?:  Erin is a strategic leader focused on business challenges and the big picture.  Her time is valuable and wants people to be direct and to the point.  She's not interested in all the technical details, but rather needs to understand the value, the risks, and how the decisions she is making today will help the business in the long term.

* **Company** - Market segment/industry? Size? On-site, distributed? Full Remote?  Erin is an application development executive, and is typically found in larger enterprises where they have large development teams (hundreds of developers). Therefore, Erin can find in most major metropolitan areas around the world where there are large enterprises.  She frequently manages a blended team of leaders (managers and directors) and developers who are in multiple locations. .

* **Team Details:** Size? Development Methodology? DevOps adoption path?  She directly manages a team of 4-5 directors who each manages teams of developers led by their respective managers. Depending on the mixture of legacy applications and systems, they have a wide variety of methodologies in play, but in general, their goal is to streamline delivery to be more efficient and predictable and are  in the process of automating development processes for building and testing their application(DevOps).

* **Influencers- where do they learn:** Erin reads Forbes, CIO magazine, Bloomberg, and other specific business publications.  She stays current on specific trends in her industry, as she is a Business Leader who happens to be responsible for technology. She rarely reads technical blogs or reviews. She attends business focused technology conferences in order to learn and also uses analyst firms like Forrester and Gartner to understand key technology trends and their business implications.

#### What - Motivations

* **Goals/motivations:**  Her top goal is predictable *Business Results*.  As a business leader, she partners with other executives to set strategy and goals for the business and then leads her team to help deliver the required technology.  She's focused on leading, growing, and improving her team to be able to deliver results. Her results are measured on her team's ability to deliver results according to the strategic plans (budget, schedule, and risk) are KPIs for her.

* **Challenges:** She is removed from the day to day work her teams do, and visibility is a major challenge.  She relies on status reports, dashboards, and other updates to get insight into how things are going, and too often learns of problems too late. She spends most of her time working on strategic alignment and planning with other executives, dealing with problems and exceptions when they are escalated to her level.  She develops organizational strategies and plans to secure budget and resources for her team.   

* **What can we do to help them achieve goals and overcome their challenges?**
1. Visibility into execution - connecting strategic vision through to actual delivery
1. Team satisfaction, as they will be able to collaborate and deliver more efficiently
1. Productivity and performance metrics help them improve their teams performance
1. Compliance and reporting

* **Why do we want to connect** Depending on the size of the company where she works, her role in the tool selection process will vary.  
- **SMB**: NA
- **MidSize**: NA
- **Large** (enterprise): In a large enterprise, the App Dev executive will have significant authority and influence over the budget. While they will **NOT** make the technical decision, they **WILL** make the business decision. They will depend on their *Directors*, *managers*, and key staff members in evaluating and selecting tools and solutions from the market.

* **Common Objections** - Why wouldn't they buy us?
1. The cost and effort to convert from an existing legacy tool to GitLab is too high. The purchase is only one part of their business case, and they need to justify the labor as much as the OPEX they are spending.
1. They don't have a large tool budget, it's a shared function or managed higher up in the organization.
1. Concerned about business risk, GitLab is an unknown compared to other more established vendors.

#### Why - Messaging

* **Discover Questions** - questions to ask
1. "How do you see technology disrupting your business?" - Learn about the underlying business system and technology they support.
1. "What challenges are you facing with keeping up with business requests?" - Almost ALL development managers don't have enough resources to keep up and are always trying to prioritize what next.
1. "How are you measuring success? Are your strategic goals to accelerate delivery or improve efficiency?" - What kind of challenges are they facing
Note: need to add a few more from Command of the Message

* **Messaging** - How describe our product and value drivers to this persona?
1. GitLab is a key to unlocking the true potential of your team to help you meet the needs of your business partners.  GitLab helps teams become more efficient and effective and stop wasting time waiting.  GitLab makes it easier for developers to maintain context of work from requirements to code to testing and even deployment.  
1. Improve visibility and insight so you can know the true status of deliverables without disrupting your team.
1. Streamline developer on boarding and bring new team members up to speed faster and more efficiently.
1. Simplify compliance, auditing, and security with by giving your developers a common work environment where they can collaborate and deliver.

* **Elevator pitch** - What's our 1-2 sentence pitch to this persona?
- Help your team become more effective and efficient. Enable them to collaborate and accelerate application delivery, eliminating wasted time switching context from one tool to another, **and** get data and insight into how the team is performing.
