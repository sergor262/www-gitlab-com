---
layout: markdown_page
title: "Public Sector Go To Market"
---

## Messaging

### **Booth Messaging / Tagline**

* **Accelerate Speed to Mission.**
*Deliver faster, better, more secure code.*



## Market-Resonating Themes & Use Case Positioning

*Note: The following themes and use cases should be viewed as sub-messages to the overall government imperatives of Digital Tranformation and IT Modernization.*
<br>

* **Speed & Efficiency**
    - Continuous Integration, Continuous Delivery, Continuous Security (*Use Cases: CI, CD*)
    - Shorter time-to-value (*Use Case: CI*)
    - Automating pipelines (*Use Case: CD*)
    - Empowering the workforce / shift to higher-level work (*Use Case: End-to-End DevOps*)
* **Security**
    - Proactive security integration (*Use Case: DevSecOps*)
    - Data / code integrity (*Use Cases: SCM, DevSecOps*)
    - Trust / confidence in pipelines (*Use Cases: CD, DevSecOps, GitOps*)
    - Disciplined data / visibility (*Use Cases: SCM, CI, End-to-End DevOps*)
* **Team Collaboration** (*Use Case: End-to-End DevOps*)
* **Innovation**
    - Cultivating innovation throughout the ecosystem (*Use Cases: SCM, End-to-End DevOps*)
    - Open Source -> innersource -> INNOVATION (*Use Cases: SCM, Agile planning, End-to-End DevOps, Cloud Native*)
    - Pre-empting disruption with pilots / prototypes / iteration (*Use Cases: SCM, Agile planning, End-to-End DevOps*)
* **Citizen Experience (CX) / Human-Centered Design** (*Use Cases: Agile planning, End-to-End DevOps*)

<br>

## Topic Focus for PubSec Segments

* **Defense & Intell Community:** "Speed to Mission" and hardened containers / CAC authentication / Security controls / Low-to-High import/export
* **Civilian:** “DevSecOps”, “IT Modernization”, and “Digital Transformation”
* **State & Local:**  “DevSecOps”, “IT Modernization”, and “Digital Transformation”
* **Education:**  “DevSecOps”, "Privacy", and "Security"


## SWOTT Analysis
https://docs.google.com/presentation/d/1yVnIZisAmXOqJMnCF5UyTSD2PcD8YfmuJ-LmABbVsaM/edit?usp=sharing

## Personas (WIP)

*Note: Within government, titles are inconsistent, so assess for function instead of title when engaging with customers.* 

### **Chief Enterprise Architect**<br>
**Function:** Provides architectural leadership, vision, strategy; focused on single greatest organizational priority; may be responsible for all aspects of IT environment (infrastructure, cloud, platform, products, applications, integration, security, UX) and the entire chain of product delivery <br> 
**Mindset:** Values implementing standards & common processes that can be generalized across entire agency (adoption) <br> 
**Pain(s):** Getting independent teams to work together, culture shift, insufficient workforce skill set, too many moving parts, too much manual work,  problems are very hard to diagnose <br>
**Value Driver(s):** Transformation/Efficiency/Speed <br>
**Differentiator(s):**  Single app/Kubernetes/Collaborative experience <br>
**Use Case(s):** SCM/Code quality/Speed & automation/DevOps results/Cloud native <br>
**Key Messages:** Standardization across agency’s orgs/high-level, long-term strategy/transformation/future-proofing/people & skill set challenges <br>

### **Director of IT** <br>

### **DevOps Lead/Director** <br>

### **Director of Security/CISO** <br>

### **Program Manager** <br>

<br>






## Content 

* **Speed to Mission** (whitepaper)
    - https://drive.google.com/open?id=0BykoPLAKUMtYZmFjZFlLa2VJZHA3Y2lnNm5DYl9tZEEwem5v
    - Gated link (for customers): https://about.gitlab.com/resources/whitepaper-speed-to-mission/
* **The Hidden Costs of DevOps Toolchains** (with guest speaker from Forrester)
    - https://drive.google.com/drive/search?q=The%20Hidden%20Costs%20of%20DevOps%20Toolchains
    - Gated link (for customers): https://about.gitlab.com/webcast/simplify-to-accelerate/
* **DevOps Powering Your Speed to Mission** (webcast)
    - https://www.youtube.com/watch?v=E_TkejEMB8s
    - Gated link (for customers): https://about.gitlab.com/webcast/devops-speed-to-mission/
* **Mission Mobility: A DevSecOps Discussion** (webcast)
    - https://www.youtube.com/watch?v=p3C2yQR246s
    - Gated link (for customers): https://about.gitlab.com/webcast/mission-mobility/

***Coming Soon:***
* **Government Matters Technology Leaders Innovation Series: Low-to-High Collaboration** (with Marc Kriz) (video)
* **GitLab and the USDS Playbook: Delivering on the Promise of Digital Services** (whitepaper)
* **Empowering Speed to Mission: A Step-by-Step Guide for Modernizing Government Agencies through DevOps** (whitepaper)
* DevSecOps: How Proactive Security Integration Reduces Your Agency's Risks & Vulnerability
* Application Security for the DevOps Lifecycle: The critical value of shifting security left (S&L focus)
* GitLab Security for the DoD & IC (DoD & IC focus)
* How to Build-In Collaboration Across Your Enterprise
* Empower Your Workforce: Automate Your DevOps & Shift to Higher Value Work
* Cross-Domain DevOps: Low-to-Side Collaboration through DevOps (DoD focus)
* Ways to Make Your DevOps Procurements More Agile
* Hardened Containers for the DOD Software Factory (DoD focus)
* Moving to the Cloud with Less Pain (S&L focus)
  
  
  

## Resources 


* **Global PubSec Marketing Account Profile Template (outside of US)** (use this to compile account-specific knowledge to share with Marketing): https://docs.google.com/document/d/1SUS0fbtVpvN4y44AT-BAiPAD32Bbq-hW-CSZco7uiUQ/edit

* **Global PubSec Speakers Bureau (outside of US):** https://drive.google.com/drive/folders/1u47kBHDh9cqOuA683k0MxzwxrZmqobzR?usp=sharing




