---
layout: handbook-page-toc
title: "Total Addressable Market"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

Total Addressable Market is the annual revenue opportunity of the entirety of GitLab’s market. The potential value of everyone worldwide that could purchase our product.

| Year | Audience | Users | ARPU | TAM |
| ---- | -------- | ----- | ---- | --- |
| 2013 | Enthusiasts | 1m | $20 | 20m |
| 2015 | Developers | 20m | $100 | 2b |
| 2019 | DevSecOps | 41m<sup>1</sup> | $350 | 14b |
| 2023<sup>2</sup> | Software<sup>3</sup> | 60m? | $1000 | 60b? |

<sup>1</sup>22m developers ([from IDC](https://www.idc.com/getdoc.jsp?containerId=US44363318)) plus security and operations

<sup>2</sup>Projection

<sup>3</sup>Additional projected audience:
 1. Product managers
 1. Management
 1. Compliance people
 1. Designers
 1. Network engineers





