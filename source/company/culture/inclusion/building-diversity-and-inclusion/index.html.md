---
layout: markdown_page
title: "Building an Inclusive Remote Culture"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Introduction

We are passionate about [all remote working](https://about.gitlab.com/company/culture/all-remote/vision/) and enabling an [inclusive work](https://about.gitlab.com/company/culture/inclusion/#fully-distributed-and-completely-connected) environment. There isn't one big activity we can take to accomplish this.  Instead it is a mix of numerous activities / behaviours combined to enable our team members feel they belong in GitLab.

Those activities / behavriors include: 

* [D&I Events](https://about.gitlab.com/company/culture/inclusion/diversity-and-inclusion-events/)
* [D&I Advisory Group](https://about.gitlab.com/company/culture/inclusion/#understanding-of-the-purpose-of-the-global-diversity--inclusion-advisory-group)
* [Our Values](https://about.gitlab.com/handbook/values/)
* [Family and Friends first, Work second](https://about.gitlab.com/handbook/values/#family-and-friends-first-work-second)
* [Inclusive language and pronouns](https://about.gitlab.com/handbook/values/#inclusive-language--pronouns)
* [Parental leave](https://gitlab.com/gitlab-com/people-group/Compensation/issues/46#note_289805708) 
* [Async Communication](https://about.gitlab.com/handbook/values/#bias-towards-asynchronous-communication)



## Elements to consider to drive those activities / behaviors 
1. Defining Diversity and Inclusion - A great place to begin is to set a foundation of the basic understanding of how your company defines these terms.  Most places you will hear them used interchangeably.  Understanding they are different is essential to executing the body of work.  As an example, you can hire as many diverse candidates but if you don't create an inclusive environment the work can be in vain.  
1. Evaluating the unique landscape of your company - What are you already doing in this space?  What is the current feedback?What are team members saying with company engagement surveys?  What are the goals you are wanting to achieve?  What are the metrics saying?
1. Naming of D&I - Although Diversity & Inclusion are often understood globally there are other terms that can be leveraged to name your efforts.  The naming of this body of work should be unique to your company.  Examples could include (i.e, Belonging, Inclusion & Collaborations, etc.)
4. D&I Strategy 
1. D&I Mission Statement -  When creating a diverse and inclusive culture, most companies will develop a mission statement to support their vision.  Your mission statement should articulate the purpose of your strategy.  In a few sentences you should be able to provide the why and the how. Be sure to take into account your company’s current overall mission and vision. It is best to align your D&I mission and vision with your organization’s overarching mission and vision. To do this, you may consider how your D&I Strategy can build on, scale, and or enhance the organization’s mission and vision.
1. Creating ERGs (Employee Resource Groups)?
1. Creating a D&I Advisory Group?
1. D&I Initiatives?
1. D&I Awards for Recognition?
1. D&I Surveys?
1. D&I Framework?
1. D&I Training for all levels?
1. Inclusive Benefits?
1. D&I Engagement?

## Tips for Managers

1. **Set aside time to show up for planned D&I events.** You might be surprised by how much seeing your face in these events validates to others that this is a worthwile use of time that is valued by the company. Seeing you in attendance also opens to the door to future conversations that might begin "I saw you in XYZ meeting, what did you think of such-and-such topic?"

