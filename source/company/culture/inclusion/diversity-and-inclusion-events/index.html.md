---
layout: markdown_page
title: "Diversity and Inclusion Events"
---

## On this page
{:.no_toc}

- TOC
{:toc}

##  Introduction

On this page you will be provided an overview of the Diversity & Inclusion events/journey that have taken place or will take place.


##  Summary of Events for 2019:

| Month    | Events                                                         | Outcome / Results         |
|----------|----------------------------------------------------------------|---------------------------|
| Apr 2019 | Hired D&I Manager                                              |
| May 2019 | D&I Breakout Sessions at Contribute 2019                       | TBC
| Jun 2019 | Monthly D&I Initiatives Call                                   | TBC
|          | [Inclusive Language Training](https://docs.google.com/presentation/d/186RK9QqOYxF8BmVS15AOKvwFpt4WglKKDR7cUCeDGkE/edit?usp=sharing)                                  | 
|          | [Published GitLab D&I Mission Statement](https://about.gitlab.com/company/culture/inclusion/#diversity--inclusion-mission-at-gitlab)
|          | [Published GitLab's Defintion of Diversity & Inclusion](https://about.gitlab.com/company/culture/inclusion/#gitlabs-definition-of-diversity--inclusion)
| Jul 2019 | Launched Greenhouse Inclusion Tool
| Aug 2019 | GitLab Pride launched                                          | TBC
|          | GitLab MIT - Minorities in Tech launched                       | TBC
|          | GitLab DiversABILITY launched                                  | TBC
|          | GitLab Women+ launched                                         | TBC
|          | [D&I Advisory Group launched](https://about.gitlab.com/company/culture/inclusion/advisory-group-members/)                                    | TBC
| Sep 2019 | [D&I Advisory Group Guidelines](https://docs.google.com/document/d/1G5OPWDQcE2yAR0R3pjoEPYXfbCwiPEJj7VAkln1B7y8/edit) published
|          | [Published ERG Guidelines](https://about.gitlab.com/company/culture/inclusion/erg-guide/)                                       | TBC
|          | [D&I Framework](https://docs.google.com/presentation/d/1OMgmYc52J02PWacw72ZM_c-R6FYni-BibAhfV514KcQ/edit?usp=sharing)                                                  | TBC
| Oct 2019 | [Slack Channels for all ERGs and D&I Advisory Group added](https://about.gitlab.com/company/culture/inclusion/#ergs---employee-resource-groups)       | TBC
| Dec 2019 | [Live Learning Inclusion Training](https://www.youtube.com/watch?v=gsQ2OsmgqVM&feature=youtu.be)                              | TBC
|          | [Received D&I Comparably Award](https://about.gitlab.com/blog/2020/01/29/comparable-awards/?utm_medium=social&utm_source=linkedin&utm_campaign=blog)


#  Summary of Events for 2020:

| Month    | Events                                                          | Outcome / Results         |
|----------|-----------------------------------------------------------------|---------------------------|
| Jan 2020 | [Live Learning Ally Training](https://www.youtube.com/watch?v=wwZeFjDc4zE&feature=youtu.be)                                     | TBC                                       
|          | D&I Analytics Dashboard - First Iteration                       | TBC                       |
| Feb 2020 | Anita Borg becomes an Official Partner                          | TBC
|          | D&I Survey via Culture Amp                                      | TBC                       |
| Mar 2020 | Unconscious Bias Training                                       | Being scheduled           |
|          | Working Mother Media Award Submission                           | TBC                       |
|          | D&I Activities at Contribute                                    | TBC                       |
| Apr      | Kickoff Women in Sales Initiatives                              | TBC                       |
| Apr      | Kickoff D&I in Engineering Initiatives                          | TBC                       |
