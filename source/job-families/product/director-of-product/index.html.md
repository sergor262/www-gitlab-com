---
layout: job_family_page
title: "Director of Product"
---

As the Director of Product, you will be responsible for managing and building
the team that focuses on a subset of GitLab's [product categories](/handbook/product/categories/#devops-stages).

## Requirements
Requirements for the role are outlined as part of the [Product Career Development Framework](/handbook/product/product-management/#product-management-career-development-framework).
- [Discovery Track](/handbook/product-development-flow/#validation-track) Skills
    - Ensures consistent execution of discovery track skills across a large team
- [Build Track](/handbook/product-development-flow/#build-track) Skills
    - Ensures consistent execution of build track skills across a large team.
    - Responsible for health of working relationships with Engineering Directors.
- Business Skills
    - Works cross stage and cross-functionally to ensure an excellent end-to-end customer experience.
    - Excellent at understanding and managing business impact across a wide range of product domains.
    - Capable of making key pricing and packaging recommendations.
- Communication Skills
    - Visible leader across teams.
    - Establishes compelling team purpose that is aligned to overall organizational vision.
    - Inspires broader team to achieve results.
    - Identifies disconnects to vision and takes appropriate action.
- [People Management](/handbook/leadership/#director-group) Skills
    - Aligns team with larger Section vision and goals.
    - Provides appropriate level of guidance and latitude to managers and individuals.
    - Experienced at hiring and at managing out underperformance.
    - Excellent at caring personally for team members and providing candid real-time feedback.
- Technical Background
    - Clear understanding of developer products
    - Experience in DevOps tools products
    - Familiarity with Git, CD, Containers and Kubernetes
- Ten to twelve years of experience
- Four years of people management experience
- You are living wherever you want and are excited about the [all remote](https://about.gitlab.com/company/culture/all-remote/) lifestyle
- Share our [values](/handbook/values), and work in accordance with them
- Ability to use GitLab

## Individual responsibility

- Make sure you have a great product team (recruit and hire, sense of progress, promote proactively, identify underperformance)
- Work on the vision with the VP of Product Strategy, VP of Product, and CEO; and communicate this vision internally and externally
- Distill the overall vision into a compelling roadmap
- Make sure the vision advances in every release and communicate this
- Communicate our vision though demo's, conference speaking, blogging, and interviews
- Work closely with Product Marketing, Sales, Engineering, etc.

## Team responsibility

- Ensure that the next milestone contains the most relevant items to customers, users, and us
- Work with customers, users, and other teams to make feature proposals enticing, actionable, and small
- Make sure the [DevOps tools](/devops-tools/) are up to date
- Keep [/direction](/direction) up to date as our high-level roadmap
- Regularly join customer and partner visits that can lead to new features
- Ensure that we translate user demands to features that make them happy but keep the product UI clean and the codebase maintainable
- Make sure the release announcements are attractive and cover everything
- Be present on social media (hacker news, twitter, stack overflow, mailing-list), especially around releases

## Primary Performance Indicators for the Role
[Stage Monthly Active Users](https://about.gitlab.com/handbook/product/metrics/#adoption)
[Category Maturity Achievement](https://about.gitlab.com/handbook/product/metrics/)

## Specialties

### Dev

The Director of Product, Dev leads the Dev parts of the
[DevOps lifecycle](https://about.gitlab.com/handbook/product/categories/#dev-section)
(e.g manage, plan, and create) and reports to the VP of Product.

### CI/CD

The Director of Product, CI/CD leads the CI/CD parts of the
[DevOps lifecycle](https://about.gitlab.com/handbook/product/categories/#cicd-section)
(e.g verify, package, and release) and reports to the VP of Product.

### Ops

The Director of Product, Ops leads the Ops parts of the
[DevOps lifecycle](https://about.gitlab.com/handbook/product/categories/#ops-section)
(e.g. Monitor and Configure) and reports to the VP of Product.

### Secure

The Director of Product, Secure leads the Secure parts of the
[DevOps lifecycle](https://about.gitlab.com/handbook/product/categories/#secure-section)
and reports to the VP of Product.

### Defend

The Director of Product, Defend leads the Defend parts of the
[DevOps lifecycle](https://about.gitlab.com/handbook/product/categories/#defend-section)
and reports to the VP of Product.

### Growth

The Director of Product, Growth leads the Growth parts of the
[DevOps lifecycle](https://about.gitlab.com/handbook/product/categories/#growth-section)
and reports to the VP of Product.

### Enablement

The Director of Product, Enablement leads the Enablement parts of the
[DevOps lifecycle](https://about.gitlab.com/handbook/product/categories/#enablement-section)
and reports to the VP of Product.

## Senior Director of Product
- Effective at hiring, coaching, and leading a team of 10+ that typically spans numerous stages, and is composed of Directors, Group Managers, and IC's
- Demonstrated ability to lead and coach product validation in brand new market areas
- Skilled at curating excellent cross-stage customer experiences
- Experience and skill managing products at all stages of the lifecycle (incubation, growth, maturity, decline), and balancing priorities and investment accordingly  
- Visible strategic leader across the company
- Skilled at big stage communications
- Capable of coaching team members on excellent communication skills 
